﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Data.Entity.Infrastructure.Design.Executor;

namespace ConsoleApp3
{
    internal class Program
    {
        //Функция для рассчета стоимости услуги при наличии счетчика
        public static double billWithCounter(double counter, double rate, double lastValue)
        {
            return (counter - lastValue) * rate;
        }
        //Функция для рассчета стоимости услуги при отсутствии счетчика

        public static double billWithoutCounter(double person, double norm, double rate)
        {
            return norm * person * rate;
        }
        public static double billWithoutCounterWithDifferentPersons(double firstHalf, double secondHalf, double norm, double rate)
        {
            return (norm/2 * firstHalf * rate) + (norm/2 * secondHalf * rate);
        }
        //Функция для получения данных из базы 
        public static dynamic getData(string query, string key)
        {
            double ret = 0;
            Database databaseObject = new Database();
            SQLiteCommand myCommand = new SQLiteCommand(query, databaseObject.myConnection);
            databaseObject.OpenConnection();
            SQLiteDataReader result = myCommand.ExecuteReader();
            if (result.HasRows)
            {
                while (result.Read())
                {
                    ret = (double)result[key];
                }
            }
            return ret;
        }
        
        static void Main(string[] args)
        {
            //Создание объекта БД
            Database databaseObject = new Database();
            //Объявление переменных
            
            double coldWaterNorm, hotWaterNorm, hotWaterHeatNorm,
                electricityNorm, coldWaterPrice, hotWaterPrice, hotWaterHeatPrice,
                electricityPrice, electricityDayPrice, electricityNightPrice, billWater,
                billHotWater, billElectricity, billElectricityDay, billElectricityNight, 
                coldWaterLastValue, hotWaterLastValue, electricityLastValueDay, electricityLastValueNight;
            string response, responseCW, responseHW, responseEE, waterCounter, hotWaterCounter, 
                electricityCounterDay, electricityCounterNight, persons, number, firstHalfPersons, secondHalfPersons;
            int id = 0;
            persons = null;
            firstHalfPersons = null;
            secondHalfPersons = null;
            bool isBasic;
            waterCounter = null; hotWaterCounter = null; electricityCounterDay = null; electricityCounterNight = null;
            //получение данных 

            coldWaterNorm = getData("SELECT value from Norms WHERE counters_id == 1", "value");
            hotWaterNorm = getData("SELECT value from Norms WHERE counters_id == 2", "value");
            hotWaterHeatNorm = getData("SELECT value from Norms WHERE counters_id == 3", "value");
            electricityNorm = getData("SELECT value from Norms WHERE counters_id == 4", "value");

            coldWaterPrice = getData("SELECT price from Rates WHERE counters_id == 1", "price");
            hotWaterPrice = getData("SELECT price from Rates WHERE counters_id == 2", "price");
            hotWaterHeatPrice = getData("SELECT price from Rates WHERE counters_id == 3", "price");
            electricityPrice = getData("SELECT price from Rates WHERE counters_id == 4", "price");
            electricityDayPrice = getData("SELECT price from Rates WHERE counters_id == 5", "price");
            electricityNightPrice = getData("SELECT price from Rates WHERE counters_id == 6", "price");


            Console.WriteLine("Введите номер лицевого счета");
            number = Console.ReadLine();
            //Поиск id клиента
            string query = $"SELECT id from Customers WHERE numberOfAccount == {number}";
            SQLiteCommand myCommand = new SQLiteCommand(query, databaseObject.myConnection);
            databaseObject.OpenConnection();
            SQLiteDataReader result = myCommand.ExecuteReader();
            if (result.HasRows)
            {
                while (result.Read())
                {
                    id = Convert.ToInt32(result["id"]);
                    //Console.WriteLine(id);
                }
            }
            //Полечение последних показаний счетчиков
            coldWaterLastValue = getData($"SELECT coldWaterLastValue from Meters WHERE customers_id == {id}", "coldWaterLastValue");
            hotWaterLastValue = getData($"SELECT hotWaterLastValue from Meters WHERE customers_id == {id}", "hotWaterLastValue");
            electricityLastValueDay = getData($"SELECT electricityLastValueDay from Meters WHERE customers_id == {id}", "electricityLastValueDay");
            electricityLastValueNight = getData($"SELECT electricityLastValueNight from Meters WHERE customers_id == {id}", "electricityLastValueNight");
            Console.WriteLine("Последние показания хвс");
            Console.WriteLine(coldWaterLastValue);
            Console.WriteLine("Последние показания гвс");
            Console.WriteLine(hotWaterLastValue);
            Console.WriteLine("Последние показания дневной электроэнергии");
            Console.WriteLine(electricityLastValueDay);
            Console.WriteLine("Последние показания ночной электроэнергии");
            Console.WriteLine(electricityLastValueNight);

            Console.WriteLine("В квартире проживает постоянное количество людей?(да/нет)");
            response = Console.ReadLine();
            if (response == "да")
            {
                persons = Console.ReadLine();
                isBasic = true;
            }
            else
            {
                Console.WriteLine("Сколько людей проживало в первую половину месяца?");
                firstHalfPersons = Console.ReadLine();
                Console.WriteLine("Сколько людей проживало во вторую половину месяца?");
                secondHalfPersons = Console.ReadLine();
                isBasic = false;
            }


            //Блок кода для рассчета стоимости хвс
            Console.WriteLine("У вас установлены счётчики на ХВС?(да/нет)");
            responseCW = Console.ReadLine();
            if (responseCW == "да")
            {
                Console.WriteLine("Введите показания ХВС");
                waterCounter = Console.ReadLine();
                billWater = billWithCounter(double.Parse(waterCounter), coldWaterPrice, coldWaterLastValue);
            }
            else
            {
                if(isBasic)
                {
                    billWater = billWithoutCounter(double.Parse(persons), coldWaterNorm, coldWaterPrice);
                }
                else
                {
                    billWater = billWithoutCounterWithDifferentPersons(double.Parse(firstHalfPersons), double.Parse(secondHalfPersons), coldWaterNorm, coldWaterPrice);
                }
            }

            //Блок кода для рассчета стоимости гвс
            Console.WriteLine("У вас установлены счётчики на ГВС?(да/нет)");
            responseHW = Console.ReadLine();
            if (responseHW == "да")
            {
                Console.WriteLine("Введите показания ГВС");
                hotWaterCounter = Console.ReadLine();
                double heatPrice = (double.Parse(hotWaterCounter) - hotWaterLastValue) * hotWaterHeatPrice;
                billHotWater = billWithCounter(double.Parse(hotWaterCounter), hotWaterPrice, hotWaterLastValue) + heatPrice;
            }
            else
            {
                if(isBasic)
                {
                    billHotWater = billWithoutCounter(double.Parse(persons), hotWaterNorm, hotWaterPrice) + (double.Parse(persons) * hotWaterNorm * hotWaterHeatPrice);
                }
                else
                {
                    billHotWater = billWithoutCounterWithDifferentPersons(double.Parse(firstHalfPersons), double.Parse(secondHalfPersons), hotWaterNorm, hotWaterPrice) + ((double.Parse(firstHalfPersons) * (hotWaterNorm/2) * hotWaterHeatPrice)) + ((double.Parse(secondHalfPersons) * (hotWaterNorm / 2) * hotWaterHeatPrice));

                }
            }

            //Блок кода для рассчета стоимости ээ
            Console.WriteLine("У вас установлены счётчики на Электроэнергию?(да/нет)");
            responseEE = Console.ReadLine();
            if (responseEE == "да")
            {
                Console.WriteLine("Введите дневные показания электроэнергии ");
                electricityCounterDay = Console.ReadLine();
                billElectricityDay = billWithCounter(double.Parse(electricityCounterDay), electricityDayPrice, electricityLastValueDay);
                Console.WriteLine("Введите ночные показания электроэнергии ");
                electricityCounterNight = Console.ReadLine();
                billElectricityNight = billWithCounter(double.Parse(electricityCounterNight), electricityNightPrice, electricityLastValueNight);
                billElectricity = billElectricityDay + billElectricityNight;
            }
            else
            {
                if(isBasic)
                {
                billElectricity = billWithoutCounter(double.Parse(persons), electricityNorm, electricityPrice);
                }
                else
                {
                    billElectricity = billWithoutCounterWithDifferentPersons(double.Parse(firstHalfPersons), double.Parse(secondHalfPersons), electricityNorm, electricityPrice);

                }
            }

            Console.WriteLine($"Счет к оплате за хвс: {billWater}");
            Console.WriteLine($"Счет к оплате за гвс: {billHotWater}");
            Console.WriteLine($"Счет к оплате за ээ: {billElectricity}");
            Console.WriteLine($"Итоговая сумма к оплате: {billWater + billHotWater + billElectricity}");



            databaseObject.CloseConnection();
            Console.ReadKey();
        }
    }
}
